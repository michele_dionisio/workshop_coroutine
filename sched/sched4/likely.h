/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   likely.h
 * Author: miki
 *
 * Created on 2 gennaio 2016, 12.15
 */

#ifndef LIKELY_H
#define LIKELY_H


#ifdef __GNUC__
#define likely(x)       __builtin_expect(!!(x), 1)
#define unlikely(x)     __builtin_expect(!!(x), 0)
#else
#define likely(x)       (x)
#define unlikely(x)     (x)
#endif

#endif /* LIKELY_H */

